#!/usr/bin/env python
"""
@package pyseas.data.co2_functions
@file pyseas/data/co2_functions.py
@author Christopher Wingard
@brief Module containing CO2 instrument family related functions
"""
import numpy as np
import scipy as sp


def co2_blank(raw_blank):
    """
    Description:

        Calculates the absorbance blank at 434 or 620 nm from the SAMI2-pCO2 instrument.

    Implemented by:

        2013-04-20: Christopher Wingard. Initial code.
        2014-02-19: Christopher Wingard. Updated comments.
        2014-02-28: Christopher Wingard. Updated to except raw blank values
                    from a sparse array.
        2017-10-31: Christopher Wingard. Updated to match correct formulation from original Matlab code.

    Usage:

        blank = co2_blank(raw_blank)

            where

        blank = corrected optical absorbance blank at 434 or 620 nm [unitless]
        raw_blank = raw optical absorbance blank at 434 or 620 nm (CO2ABS1_L0 or CO2ABS2_L0, respectively) [counts]

    References:

        OOI (2012). Data Product Specification for Partial Pressure of CO2 in
            Seawater. Document Control Number 1341-00510.
            https://alfresco.oceanobservatories.org/ (See: Company Home >>
            OOI >> Cyberinfrastructure >> Data Product Specifications >>
            1341-00490_Data_Product_SPEC_PCO2WAT_OOI.pdf)
    """
    blank = raw_blank / 16384.
    return blank


def co2_thermistor(traw):
    """
    Description:

        Convert the thermistor data from counts to degrees Centigrade from the
        pCO2 instrument.

    Implemented by:

        2013-04-20: Christopher Wingard. Initial code.
        2014-02-19: Christopher Wingard. Updated comments.

    Usage:

        therm = co2_thermistor(traw)

            where

        therm = converted thermistor temperature [degC]
        traw = raw thermistor temperature (CO2THRM_L0) [counts]

    References:

        OOI (2012). Data Product Specification for Partial Pressure of CO2 in
            Seawater. Document Control Number 1341-00510.
            https://alfresco.oceanobservatories.org/ (See: Company Home >>
            OOI >> Cyberinfrastructure >> Data Product Specifications >>
            1341-00490_Data_Product_SPEC_PCO2WAT_OOI.pdf)
    """
    # convert raw thermistor readings from counts to degrees Centigrade
    rt = np.log((traw / (4096. - traw)) * 17400.)
    inv = 0.0010183 + 0.000241 * rt + 0.00000015 * rt**3
    therm = (1. / inv) - 273.15
    return therm


def co2_pco2wat(ar434, ar620, therm, calt, cala, calb, calc, k434, k620):
    """
    Description:

        OOI Level 1 Partial Pressure of CO2 (pCO2) in seawater core data
        product, which is calculated from the Sunburst SAMI2 CO2 instrument
        (PCO2W).

    Implemented by:

        20??-??-??: J. Newton (Sunburst Sensors, LLC). Original Matlab code.
        2013-04-20: Christopher Wingard. Initial python code.
        2014-02-19: Christopher Wingard. Updated comments.
        2014-03-19: Christopher Wingard. Optimized.
        2017-10-31: Christopher Wingard. Updated to match correct formulation from original Matlab code.
        2018-03-02: Christopher Wingard. Updated to calculate pCO2 correctly as original vendor supplied code did not.

    Usage:

        pco2 = co2_pco2wat(ar434, ar620, therm, calt, cala, calb, calc, k434, k602)

            where

        pco2 = measured pco2 in seawater (PCO2WAT_L1) [uatm]
        ar434 = raw absorbance ratio at 434 nm
        ar620 = raw absorbance ratio at 620 nm
        therm = PCO2W thermistor temperature (CO2THRM_L1) [degC]
        calt = Instrument specific calibration coefficient for temperature
        cala = Instrument specific calibration coefficient for the pCO2 measurements
        calb = Instrument specific calibration coefficient for the pCO2 measurements
        calc = Instrument specific calibration coefficient for the pCO2 measurements
        k434 = Corrected blank measurements at 434 nm [unitless]
        k620 = Corrected blank measurements to 620 nm [unitless]

    References:

        OOI (2012). Data Product Specification for Partial Pressure of CO2 in
            Seawater. Document Control Number 1341-00510.
            https://alfresco.oceanobservatories.org/ (See: Company Home >>
            OOI >> Cyberinfrastructure >> Data Product Specifications >>
            1341-00490_Data_Product_SPEC_PCO2WAT_OOI.pdf)
    """
    # set the e constants, values provided by Sunburst
    e1 = 0.0043
    e2 = 2.136
    e3 = 0.2105

    # correct the raw absorbance ratios for the blanks, and calculate the final absorbance ratio
    a434 = -1. * sp.log10(ar434 / 16384. / k434)  # 434 absorbance ratio
    a620 = -1. * sp.log10(ar620 / 16384. / k620)  # 620 absorbance ratio
    ratio = a620 / a434                           # Absorbance ratio

    # calculate pCO2
    v1 = ratio - e1
    v2 = e2 - e3 * ratio
    rco21 = -1. * sp.log10(v1 / v2)
    rco22 = (therm - calt) * 0.008 + rco21
    tcoeff = 0.0075778 - 0.0012389 * rco22 - 0.00048757 * rco22**2
    tcor_rco2 = rco21 + tcoeff * (therm - calt)
    pco2 = 10.**((-1. * calb + (calb**2 - (4. * cala * (calc - tcor_rco2)))**0.5) / (2. * cala))

    return np.real(pco2)


def co2_ppressure(xco2, p, std=1013.25):
    """
    Description:

        OOI Level 1 Partial Pressure of CO2 in Air (PCO2ATM) or Surface
        Seawater (PCO2SSW) core date product is computed by using an
        equation that incorporates the Gas Stream Pressure (PRESAIR) and the
        CO2 Mole Fraction in Air or Surface Seawater (XCO2ATM or XCO2SSW,
        respectively). It is computed using data from the pCO2 air-sea (PCO2A)
        family of instruments.

    Implemented by:

        2014-10-27: Christopher Wingard. Initial python code.

    Usage:

        ppres = co2_ppressure(xco2, p, std)

            where

        ppres = partial pressure of CO2 in air or surface seawater [uatm]
                (PCO2ATM_L1 or PCO2SSW_L1)
        xco2 = CO2 mole fraction in air or surface seawater [ppm] (XCO2ATM_LO
               or XCO2SSW_L0)
        p = gas stream pressure [mbar] (PRESAIR_L0)
        std = standard atmospheric pressure set to default of 1013.25 [mbar/atm]

    References:

        OOI (2012). Data Product Specification for Partial Pressure of CO2 in
            Air and Surface Seawater. Document Control Number 1341-00260.
            https://alfresco.oceanobservatories.org/ (See: Company Home >>
            OOI >> Cyberinfrastructure >> Data Product Specifications >>
            1341-00260_Data_Product_SPEC_PCO2ATM_PCO2SSW_OOI.pdf)
    """
    ppres = xco2 * p / std
    return ppres


def co2_co2flux(pco2w, pco2a, u10, t, s):
    """
    Description:

        OOI Level 2 core date product CO2FLUX is an estimate of the CO2 flux
        from the ocean to the atmosphere. It is computed using data from the
        pCO2 air-sea (PCO2A) and bulk meteorology (METBK) families of
        instruments.

    Implemented by:

        2012-03-28: Mathias Lankhorst. Original Matlab code.
        2013-04-20: Christopher Wingard. Initial python code.

    Usage:

        flux = co2_co2flux(pco2w, pco2a, u10, t, s)

            where

        flux = estimated flux of CO2 from the ocean to atmosphere [mol m-2 s-1]
               (CO2FLUX_L2)
        pco2w = partial pressure of CO2 in sea water [uatm] (PCO2SSW_L1)
        pco2a = partial pressure of CO2 in air [uatm] (PCO2ATM_L1)
        u10 = normalized wind speed at 10 m height from METBK [m s-1] (WIND10M_L2)
        t = sea surface temperature from METBK [deg_C] (TEMPSRF_L1)
        s = sea surface salinity from METBK [psu] (SALSURF_L2)

    References:

        OOI (2012). Data Product Specification for Flux of CO2 into the
            Atmosphere. Document Control Number 1341-00270.
            https://alfresco.oceanobservatories.org/ (See: Company Home >>
            OOI >> Cyberinfrastructure >> Data Product Specifications >>
            1341-00270_Data_Product_SPEC_CO2FLUX_OOI.pdf)
    """
    # convert micro-atm to atm
    pco2a = pco2a / 1.0e6
    pco2w = pco2w / 1.0e6

    # Compute Schmidt number (after Wanninkhof, 1992, Table A1)
    sc = 2073.1 - (125.62 * t) + (3.6276 * t**2) - (0.043219 * t**3)

    # Compute gas transfer velocity (after Sweeney et al. 2007, Fig. 3 and Table 1)
    k = 0.27 * u10**2 * np.sqrt(660.0 / sc)

    # convert cm h-1 to m s-1
    k = k / (100.0 * 3600.0)

    # Compute the absolute temperature
    at = t + 273.15

    # Compute solubility (after Weiss 1974, Eqn. 12 and Table I).
    # Note that there are two versions, one for units per volume and
    # one per mass. Here, the volume version is used.
    # mol atm-1 m-3
    at100 = at / 100
    k0 = 1000 * np.exp(-58.0931 + (90.5069 * (100/at)) + (22.2940 * np.log(at100)) +
                       s * (0.027766 - (0.025888 * at100) + (0.0050578 * at100**2)))

    # mol atm-1 kg-1
    #k0 = np.exp(-60.2409 + (93.4517 * (100/T)) + (23.3585 * np.log(at100)) +
    #            s * (0.023517 - (0.023656 * at100) + (0.0047036 * at100**2)))

    # Compute flux (after Wanninkhof, 1992, eqn. A2)
    flux = k * k0 * (pco2w - pco2a)
    return flux
